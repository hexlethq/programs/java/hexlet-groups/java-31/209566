package exercise;

class Point {
    // BEGIN
    public static int[] makePoint(int x, int y) {
        int[] point = new int[2];
        point[0] = x;
        point[1] = y;
        return point;
    }

    public static int getX(int[] Point) {
        return Point[0];
    }

    public static int getY(int[] Point) {
        return Point[1];
    }

    public static String pointToString(int[] Point) {
        return "(" + Point[0] + ", " + Point[1] + ")";
    }

    public static int getQuadrant(int[] Point) {
        int x = Point[0];
        int y = Point[1];

        if (x > 0) {
            if (y > 0) {
                return 1;
            } else if (y < 0){
                return  4;}
        } else if (x < 0) {
            if (y > 0) {
                return 2;
            } else if (y < 0){
                return 3;}
        }
        return 0;
    }
// END
}