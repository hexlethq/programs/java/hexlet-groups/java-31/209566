package exercise;

import java.util.ArrayList;

class App {
    // BEGIN
    public static int getIndexOfMaxNegative(int[] array) {

        //boolean hasNegative = false;

        //if (array.length == 0){
        //    return -1;
        //}

        ///for (int i = 0; i < array.length; i++){
        //   if (array[i] < 0){
        //       hasNegative = true;
        //       break;
        //   }
        //}
        //if(hasNegative == false){
        //    return -1;
        //}

        int indexOfMaxNegative = -1;
        int maxNegativeValue = Integer.MIN_VALUE;

        for (int i = 0; i < array.length; i++) {
            int currentValue = array[i];
            if (currentValue > maxNegativeValue & currentValue < 0) {
                indexOfMaxNegative = i;
                maxNegativeValue = currentValue;

            }
        }
        return indexOfMaxNegative;
    }

    public static int[] getElementsLessAverage(int[] array) {

        if (array.length == 0) {
            return array = new int[0];
        }

        float mid = 0;

        for (int i = 0; i < array.length; i++) {
            mid = mid + array[i];
        }
        mid = mid / array.length;
        int resultSize = 0;

        for (int i = 0; i < array.length; i++) {
            if (array[i] < mid) {
                resultSize = resultSize + 1;
            }
        }
        int[] result = new int[resultSize];
        for (int i = 0, j = 0; i < resultSize; i++) {
            if (array[i] < mid) {
                result[j] = array[i];
                j++;
            }
        }
        return result;
    }


    // END
}


