package exercise;

class App {
    // BEGIN
    public static String getAbbreviation(String word) {
        word = word.trim();
        String str = word.substring(0, 1);

        for (int i = 1; i < word.length(); i++) {
            if (word.charAt(i - 1) == ' ' && word.charAt(i) != ' ') {
                str = str + word.toUpperCase().charAt(i);
            }
        }
        return str;
    }
    // END
}
