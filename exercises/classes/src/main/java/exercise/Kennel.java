package exercise;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Kennel {
    static String[][] puppiesHomes = new String[2][2];
    static Integer countPuppiesInHomes = 0;


    public static String[][] newPuppiesHomes() {
        String[][] tempPuppiesHomes = new String[puppiesHomes.length][2];
        for (int i = 0; i < puppiesHomes.length; i++) {
            tempPuppiesHomes[i][0] = puppiesHomes[i][0];
            tempPuppiesHomes[i][1] = puppiesHomes[i][1];
        }
        puppiesHomes = new String[puppiesHomes.length + 2][2];
        for (int i = 0; i < tempPuppiesHomes.length; i++) {
            puppiesHomes[i][0] = tempPuppiesHomes[i][0];
            puppiesHomes[i][1] = tempPuppiesHomes[i][1];
        }
        return (puppiesHomes);
    }
    
    public static void addPuppy(String[] puppy) {
        if (countPuppiesInHomes > puppiesHomes.length) {
            newPuppiesHomes();
        }
        puppiesHomes[countPuppiesInHomes][0] = puppy[0];
        puppiesHomes[countPuppiesInHomes][1] = puppy[1];
        countPuppiesInHomes++;
    }

    public static void addSomePuppies(String[][] allNamePuppies) {

        for (int i = 0; i < allNamePuppies.length; i++) {
            if (countPuppiesInHomes >= puppiesHomes.length) {
                newPuppiesHomes();
            }
            puppiesHomes[countPuppiesInHomes][0] = allNamePuppies[i][0];
            puppiesHomes[countPuppiesInHomes][1] = allNamePuppies[i][1];

            countPuppiesInHomes++;
        }
    }

    public static Integer getPuppyCount() {

        return (Kennel.countPuppiesInHomes);

    }

    public static boolean isContainPuppy(String namePuppy) {
        boolean containPuppy = false;

        for (int i = 0; i < puppiesHomes.length; i++) {
            if (puppiesHomes[i][0] == namePuppy) {
                containPuppy = true;
            }
        }
        return containPuppy;
    }

    public static String[][] getAllPuppies() {
        String[][] allPappies = new String[countPuppiesInHomes][2];
        for (int i = 0; i < countPuppiesInHomes; i++) {
            allPappies[i][0] = puppiesHomes[i][0];
            allPappies[i][1] = puppiesHomes[i][1];
        }
        return (allPappies);
    }


    public static String[] getNamesByBreed(String breedPuppy) {
        int countNameWithBreed = 0;
        for (int i = 0; i < puppiesHomes.length; i++) {
            if (puppiesHomes[i][1] == breedPuppy) {
                countNameWithBreed = countNameWithBreed + 1;
            }
        }
        String[] NamesByBreed = new String[countNameWithBreed];
        int j = 0;
        for (int i = 0; i < puppiesHomes.length; i++) {
            if (puppiesHomes[i][1] == breedPuppy) {
                NamesByBreed[j] = puppiesHomes[i][0];
                j++;
            }
        }

        return NamesByBreed;
    }

    public static void resetKennel() {

        countPuppiesInHomes = 0;
    }

}

// BEGIN

// END
