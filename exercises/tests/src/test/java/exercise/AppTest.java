package exercise;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class AppTest {

    @Test
    void testTake() {
        // BEGIN
        List<Integer> actual = new ArrayList<>(Arrays.asList(0, 5, 7, 8));
        List<Integer> expected = new ArrayList<>(Arrays.asList(0, 5));
        Assertions.assertIterableEquals(expected, App.take(actual, 2));

        List<Integer> expected2 = new ArrayList<>();
        Assertions.assertIterableEquals(expected2, App.take(actual, 0));

        Assertions.assertIterableEquals(actual, App.take(actual, 9));

        // END
    }
}