package exercise;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

// BEGIN
public class App {

    public static boolean scrabble(String mixLetter, String word) {
        boolean result = false;

        if (mixLetter.length() < word.length()) {
            return false;
        }
        List<Character> mixLetters = new ArrayList<>();
        for (int i = 0; i < mixLetter.length(); i++) {
            mixLetters.add(mixLetter.charAt(i));
        }
        List<Character> wordLetters = new ArrayList<>();
        for (int i = 0; i < word.length(); i++) {
            wordLetters.add(word.toLowerCase().charAt(i));
        }
        List<Character> listResult = new ArrayList<>();


        for (int i = 0; i < wordLetters.size(); i++) {
            if (!mixLetters.remove(wordLetters.get(i))) {
                return false;
            }
        }
        return true;
    }
}
//END
