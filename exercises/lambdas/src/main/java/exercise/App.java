package exercise;

import java.util.Arrays;
import java.util.stream.Stream;

// BEGIN
public class App {
    public static String[][] enlargeArrayImage(String[][] array) {
        return Arrays.stream(array)
                .flatMap(str -> Stream.of(str, str))
                .map(str2 -> Arrays.stream(str2).flatMap(str -> Stream.of(str, str)))
                .map(str -> str.toArray(String[]::new))
                .toArray(String[][]::new);

    }
}
// END
