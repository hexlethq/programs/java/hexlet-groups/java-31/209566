package exercise;

import java.util.stream.Collectors;
import java.util.Arrays;
import java.util.stream.Stream;

// BEGIN
public class App {
    public static String getForwardedVariables(String environment){
        Stream<String> streamFromConfigFile = Stream.of(environment.split("\n"));
        return Arrays.stream(streamFromConfigFile
                        .filter(x -> x.startsWith("environment"))
                        .map(x -> x.replaceAll("\"", " "))
                        .map(x -> x.replaceAll(",", " "))
                        .flatMap((y) -> Arrays.stream(y.split(" "))).toArray(String[]::new))
                        .filter(x -> x.startsWith("X_FORWARDED_"))
                        .map(x -> x.replaceAll("X_FORWARDED_", ""))
                        .sequential()
                        .collect(Collectors.joining(","));

    }
}
//END
