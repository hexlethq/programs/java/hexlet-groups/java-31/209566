package exercise;



class App {
    // BEGIN
    public  static int[] reverse(int[] number ){
       int length = number.length;
       int [] result = new int[length];
       if (number.length == 0){
           return number;
       }
        for (int i = length-1; i >=0; i--){
            result[i] = number[length-i-1];
        }
        return result;
    }

    public  static int mult(int[] array ){
        int result = 1;
        for (int i = 0; i < array.length; i++){
            result = result * array[i];
        }
        return result;
    }

    // END
}
