package exercise;

import java.util.List;
import java.util.Arrays;
import java.util.Collection;

// BEGIN
public class App {
    public static long getCountOfFreeEmails(List<String> emailList) {

        long count = emailList.stream()
                .filter(str -> str.contains("@gmail.com")
                        || str.contains("@yandex.ru")
                        || str.contains("@hotmail.com"))
                .count();
        return count;
    }

}
// END
