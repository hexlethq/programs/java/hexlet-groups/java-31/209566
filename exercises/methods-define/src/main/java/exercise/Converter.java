package exercise;

class Converter {
    // BEGIN
 public static int convert(int numberToConvert, String directionToConvert){
     switch (directionToConvert){
         case "b":
             return numberToConvert * 1024;

         case "Kb":
             return numberToConvert / 1024;

         default:
             return 0;
     }

 }

 public static void main( String[] args){

     System.out.println("10 Kb = "+ convert(10, "b")+" b");
 }
    // END
}
