package exercise;

class Triangle {
    // BEGIN
    public static double getSquare(double firstSide, double secondSide, double angle){
        double radianAngle = angle / 180 * Math.PI;
        return firstSide * secondSide / 2 * Math.sin(radianAngle);
    }

    public static void main( String[] args){

        System.out.println(getSquare(4, 5, 45));
    }
    // END
}
