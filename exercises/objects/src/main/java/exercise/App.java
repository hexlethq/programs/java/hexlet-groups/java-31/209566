package exercise;

import java.sql.Array;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.*;

class App {
    // BEGIN
    public static String buildList(String[] listArray) {
        if (listArray.length == 0) {
            return ("");
        }
        StringBuilder htmlList = new StringBuilder();

        htmlList = htmlList.append("<ul>");
        for (int i = 0; i < listArray.length; i++) {
            htmlList = htmlList.append("\n  <li>");
            htmlList = htmlList.append(listArray[i]);
            htmlList = htmlList.append("</li>");
        }
        htmlList = htmlList.append("\n</ul>");
        return (htmlList.toString());
    }

    public static String getUsersByYear(String[][] users, int year) {

        List<String> tempName = new ArrayList<String>();

        for (int i = 0; i < users.length; i++) {

            String tempYear = users[i][1].substring(0, 4);
            if (Integer.parseInt(tempYear) == year) {
                tempName.add(users[i][0]);
            }
        }
        String[] namesOfTheSearchingYears = new String[tempName.size()];
        tempName.toArray(namesOfTheSearchingYears);


        return (buildList(namesOfTheSearchingYears));
    }
    // END


    // Это дополнительная задача, которая выполняется по желанию.
    public static String getYoungestUser(String[][] users, String date) throws Exception {
        // BEGIN
        return ("");
        // END
    }
}
