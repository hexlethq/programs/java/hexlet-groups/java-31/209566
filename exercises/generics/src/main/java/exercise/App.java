package exercise;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.Map.Entry;


// BEGIN
public class App {
    public static List<Map<String, String>> findWhere(List<Map<String, String>> booksAll, Map<String, String> filter) {
        List<Map<String, String>> answer = new ArrayList<>();
        if (booksAll.size() == 0) {
            return answer;
        }
        for (Map<String, String> book : booksAll) {
            Integer filtersPassed = 0;
            for (Entry<String, String> bookItem : book.entrySet()) {
                for (Entry<String, String> filterItem : filter.entrySet()) {
                    if (bookItem.getKey().equals(filterItem.getKey())
                            && bookItem.getValue().equals(filterItem.getValue())) {
                        filtersPassed += 1;
                    }
                }
            }
            if (filter.size() == filtersPassed && !answer.contains(book)) {
                answer.add(book);
            }
        }
        return answer;
    }
}

//END
