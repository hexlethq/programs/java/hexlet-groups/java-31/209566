package exercise;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

// BEGIN
public class App {
    public static LinkedHashMap<String, String> genDiff(Map<String, Object> bookFirst, Map<String, Object> bookSecond) {
        Map<String, String> result = new LinkedHashMap<>();
        for (String key: bookFirst.keySet()) {
            if (bookFirst.get(key).equals(bookSecond.get(key))) {
                result.put(key, "unchanged");
            } else {
                result.put(key, "changed");
            }
            if (bookSecond.remove(key) == null) {
                result.put(key, "deleted");
            }
        }
        for (String key: bookSecond.keySet()) {
            result.put(key, "added");
        }

        return (LinkedHashMap<String, String>) result;

    }
}
//END
