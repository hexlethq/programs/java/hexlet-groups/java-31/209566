package exercise;

class App {
    // BEGIN
    public static String getTypeOfTriangle(int sideFirst, int sideSecond, int sideThird) {
        String result = "";

        if ((sideFirst + sideSecond) <= sideThird
                || (sideFirst + sideThird) <= sideSecond
                || (sideThird + sideSecond) <= sideFirst) {
            result = "Треугольник не существует";
        } else if ((sideFirst == sideSecond)
                && (sideFirst == sideThird)
                && (sideSecond == sideThird)) {
            result = "Равносторонний";
        } else if ((sideFirst != sideSecond)
                && (sideFirst != sideThird)
                && (sideSecond != sideThird)) {
            result = "Разносторонний";
        } else if ((((sideFirst == sideSecond) && sideFirst != sideThird)
                || ((sideFirst == sideThird) && sideThird != sideSecond)
                || ((sideSecond == sideThird) && sideFirst != sideSecond))) {
            result = "Равнобедренный";
        }
        return result;
        // END
    }
}
