package exercise;

import java.util.HashMap;
import java.util.Map;

public class App {
    public static Map getWordCount(String sentence) {
        Map<String, Integer> dictionary = new HashMap<String, Integer>();

        if (!sentence.equals("")) {
            String[] splitWords;
            splitWords = sentence.split(" ");

            for (String word : splitWords) {
                Integer oldCount = dictionary.get(word);
                if (oldCount == null) {
                    oldCount = 0;
                }
                dictionary.put(word, oldCount + 1);
            }
        }
        return dictionary;
    }

    public static String toString (Map<String, Integer> map) {
        String str = "{}";
        if (map.size() != 0) {
            str = "{\n";
            for (Map.Entry<String, Integer> entry : map.entrySet()) {
                String key = entry.getKey();
                Integer value = entry.getValue();
                str += ("  " + key+": "+ value + "\n");
            }
            str += "}";
        }
        return str;
    }
}
//END
