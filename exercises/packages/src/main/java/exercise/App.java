// BEGIN
package exercise;

import exercise.geometry.Segment;
import exercise.geometry.Point;

public class App {
    public static double[] getMidpointOfSegment(double[][] segment) {
        double[] midPoint = new double[2];

        midPoint[0] = ((segment[0][0] + segment[1][0]) / 2);
        midPoint[1] = ((segment[0][1] + segment[1][1]) / 2);

        return midPoint;
    }

    public static double[][] reverse(double[][] segment) {
        double[][] reversSegment = new double[2][2];

        reversSegment[0][0] = segment[1][0];
        reversSegment[0][1] = segment[1][1];
        reversSegment[1][0] = segment[0][0];
        reversSegment[1][1] = segment[0][1];

        return reversSegment;
    }

    public static boolean isBelongToOneQuadrant(double[][] segment){
        boolean inOneQuadrant = false;
        double[] point1 = new double[2];
        double[] point2 = new double[2];

        point1[0] = segment[0][0];
        point1[1] = segment[0][1];

        point2[0] = segment[1][0];
        point2[1] = segment[1][1];

        if (point1[0] > 0 && point2[0] > 0){
            if (point1[1] > 0 && point2[1] > 0){
                inOneQuadrant = true;
            }
            else if (point1[1] < 0 && point2[1] < 0)
                inOneQuadrant = true;
        }

        else if (point1[0] < 0 && point2[0] > 0){
            if (point1[1] > 0 && point2[1] > 0){
                inOneQuadrant = true;
            }
            else if (point1[1] < 0 && point2[1] < 0)
                inOneQuadrant = true;
        }

        return inOneQuadrant;
    }
}


// END
