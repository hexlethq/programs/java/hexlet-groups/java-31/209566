// BEGIN
package exercise.geometry;
// END

public class Segment {
    public static double[][] makeSegment(double[] point1, double[] point2) {
        double[][] segment = new double[2][2];
        segment[0][0] = point1[0];
        segment[0][1] = point1[1];
        segment[1][0] = point2[0];
        segment[1][1] = point2[1];

        return segment;
    }

    public static double[] getBeginPoint(double[][] segment) {
        double[] startSegment = new double[2];
        startSegment[0] = segment[0][0];
        startSegment[1] = segment[0][1];

        return startSegment;
    }

    public static double[] getEndPoint(double[][] segment) {
        double[] endSegment = new double[2];

        endSegment[0] = segment[1][0];
        endSegment[1] = segment[1][1];

        return endSegment;
    }

}