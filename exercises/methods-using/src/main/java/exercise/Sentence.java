package exercise;

class Sentence {
    public static void printSentence(String sentence) {
        // BEGIN

        if (sentence.endsWith("!")) {
            System.out.print(sentence.toUpperCase());
        }
        else{
            System.out.print(sentence.toLowerCase());
        }
        // END
    }
}
